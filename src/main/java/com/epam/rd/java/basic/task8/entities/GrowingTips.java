package com.epam.rd.java.basic.task8.entities;

public class GrowingTips {

    private String temperature;

    private String lighting;

    private String watering;

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public String getWatering() {
        return watering;
    }

    public void setWatering(String watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                '}';
    }
}
