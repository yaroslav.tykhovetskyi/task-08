package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flower;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

public class DOMFileWriter {

    private final String xmlFileName;

    public DOMFileWriter(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        File file = new File(xmlFileName);

    }

    public void writeDataIntoXML(String rootElement, List<Flower> data){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder builder = dbf.newDocumentBuilder();

            Document document = builder.newDocument();

//            creating the root
            Element root = document.createElementNS("http://www.nure.ua", rootElement);
            root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

            document.appendChild(root);

            for (Flower f : data){
                root.appendChild(getFlower(document, f));
            }

//            console print
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

//            configs
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);

            //печатаем в консоль или файл
            StreamResult file = new StreamResult(new File(this.xmlFileName));

            transformer.transform(source, file);

        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private static Node getFlower(Document doc, Flower f) {
        Element flower = doc.createElement("flower");

        flower.appendChild(getFlowerElements(doc, flower, "name", f.getName()));
        flower.appendChild(getFlowerElements(doc, flower, "soil", f.getSoil()));
        flower.appendChild(getFlowerElements(doc, flower, "origin", f.getOrigin()));

        Element visualParameters = doc.createElement("visualParameters");
        visualParameters.appendChild(getFlowerElements(doc, visualParameters, "stemColour", f.getVisualParameters().getStemColour()));
        visualParameters.appendChild(getFlowerElements(doc, visualParameters, "leafColour", f.getVisualParameters().getLeafColour()));

        Element aveLen = doc.createElement("aveLenFlower");
        aveLen.setAttribute("measure", "cm");
        aveLen.appendChild(doc.createTextNode(f.getVisualParameters().getAveLenFlower()));
        visualParameters.appendChild(aveLen);
        flower.appendChild(visualParameters);

        Element growingTips = doc.createElement("growingTips");

        Element tempreture = doc.createElement("tempreture");
        tempreture.setAttribute("measure", "celcius");
        tempreture.appendChild(doc.createTextNode(f.getGrowingTips().getTemperature()));
        growingTips.appendChild(tempreture);

        Element lighting = doc.createElement("lighting");
        lighting.setAttribute("lightRequiring", f.getGrowingTips().getLighting());
        growingTips.appendChild(lighting);

        Element watering = doc.createElement("watering");
        watering.setAttribute("measure", "mlPerWeek");
        watering.appendChild(doc.createTextNode(f.getGrowingTips().getWatering()));
        growingTips.appendChild(watering);

        flower.appendChild(growingTips);

        flower.appendChild(getFlowerElements(doc, flower, "multiplying", f.getMultiplying()));

        return flower;
    }

    private static Node getFlowerElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }
}
