package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List<Flower> readValues(){
		File file = new File(this.xmlFileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		List<Flower> flowerList = new ArrayList<>();
		try {
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			builder = dbf.newDocumentBuilder();
			Document document = builder.parse(file);

			NodeList nodeList = document.getElementsByTagName("flower");

			for (int i = 0; i < nodeList.getLength(); i++) {
				flowerList.add(getFlower(nodeList.item(i)));
			}

		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
		return flowerList;
	}

	private static Flower getFlower(Node node){
		Flower flower = new Flower();
		flower.setVisualParameters(new VisualParameters());
		flower.setGrowingTips(new GrowingTips());

		if (node.getNodeType() == Node.ELEMENT_NODE){
			Element element = (Element)node;
			flower.setName(getTagValue("name", element));
			flower.setSoil(getTagValue("soil", element));
			flower.setOrigin(getTagValue("origin", element));

			flower.getVisualParameters().setStemColour(getTagValue("stemColour", element));
			flower.getVisualParameters().setLeafColour(getTagValue("leafColour", element));
			flower.getVisualParameters().setAveLenFlower(getTagValue("aveLenFlower", element));

			flower.getGrowingTips().setTemperature(getTagValue("tempreture", element));
			flower.getGrowingTips().setLighting(element.getElementsByTagName("lighting").item(0).getAttributes().getNamedItem("lightRequiring").getTextContent());
			flower.getGrowingTips().setWatering(getTagValue("watering", element));

			flower.setMultiplying(getTagValue("multiplying", element));
		}
		return flower;
	}

	private static String getTagValue(String tag, Element element){
		return element.getElementsByTagName(tag).item(0).getTextContent();
	}
/*
* <flower>
        <name>Rose</name>
        <soil>дерново-подзолистая</soil>
        <origin>China</origin>
        <visualParameters>
            <stemColour>green</stemColour>
            <leafColour>green</leafColour>
            <aveLenFlower measure="cm">10</aveLenFlower>
        </visualParameters>
        <growingTips>
            <tempreture measure="celcius">25</tempreture>
            <lighting lightRequiring="yes"/>
            <watering measure="mlPerWeek">110</watering>
        </growingTips>
        <multiplying>черенки</multiplying>
    </flower>*/
}
