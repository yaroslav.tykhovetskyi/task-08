package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;

	private List<Flower> result;
	private StringBuilder currentTagValue = new StringBuilder();
	Flower currentFlower;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	@Override
	public void startDocument(){
		result = new ArrayList<>();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes){
		currentTagValue.setLength(0);

		if (qName.equals("flower")){
			currentFlower = new Flower();
			currentFlower.setGrowingTips(new GrowingTips());
			currentFlower.setVisualParameters(new VisualParameters());
		}

		if (qName.equalsIgnoreCase("lighting")){
			String lightRequiring = attributes.getValue("lightRequiring");
			currentFlower.getGrowingTips().setLighting(lightRequiring);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName){

		if (qName.equalsIgnoreCase("name")){
			currentFlower.setName(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("soil")){
			currentFlower.setSoil(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("origin")){
			currentFlower.setOrigin(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("stemColour")){
			currentFlower.getVisualParameters().setStemColour(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("leafColour")){
			currentFlower.getVisualParameters().setLeafColour(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("aveLenFlower")){
			currentFlower.getVisualParameters().setAveLenFlower(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("tempreture")){
			currentFlower.getGrowingTips().setTemperature(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("watering")){
			currentFlower.getGrowingTips().setWatering(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("multiplying")){
			currentFlower.setMultiplying(currentTagValue.toString());
		}

		if (qName.equalsIgnoreCase("flower")){
			result.add(currentFlower);
		}

	}
/*<name>Rose</name>
        <soil>дерново-подзолистая</soil>
        <origin>China</origin>
        <visualParameters>
            <stemColour>green</stemColour>
            <leafColour>green</leafColour>
            <aveLenFlower measure="cm">10</aveLenFlower>
        </visualParameters>
        <growingTips>
            <tempreture measure="celcius">25</tempreture>
            <lighting lightRequiring="yes"/>
            <watering measure="mlPerWeek">110</watering>
        </growingTips>
        <multiplying>черенки</multiplying>*/
	@Override
	public void characters(char[] ch, int start, int length){
		currentTagValue.append(ch, start, length);
	}

	public void readValues(){
		SAXParserFactory factory = SAXParserFactory.newInstance();

		try {
			factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			SAXParser parser = factory.newSAXParser();

			parser.parse(new File(this.xmlFileName), this);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	public List<Flower> getResult() {
		return result;
	}
}