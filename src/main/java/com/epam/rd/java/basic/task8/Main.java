package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entities.Flower;

import java.util.List;

public class Main {
	
	public static void main(String[] args){
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> flowers = domController.readValues();
		System.out.println(flowers);
		// sort (case 1)
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		DOMFileWriter writer = new DOMFileWriter(outputXmlFile);
		writer.writeDataIntoXML("flowers", flowers);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.readValues();
		List<Flower> flowers1 = saxController.getResult();
		System.out.println(flowers1);

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		DOMFileWriter writer2 = new DOMFileWriter(outputXmlFile);
		writer2.writeDataIntoXML("flowers", flowers);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.readValues();
		List<Flower> flowers2 = staxController.getResult();
		System.out.println(flowers2);
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		DOMFileWriter writer3 = new DOMFileWriter(outputXmlFile);
		writer3.writeDataIntoXML("flowers", flowers);
	}

}
