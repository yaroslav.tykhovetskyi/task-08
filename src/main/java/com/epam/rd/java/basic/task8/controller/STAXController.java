package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final List<Flower> result = new ArrayList<>();
	Flower currentFlower;

	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void readValues(){
		XMLInputFactory factory = XMLInputFactory.newFactory();
		try {
			XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(this.xmlFileName));

			while (reader.hasNext()){
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()){
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()){
						case "flower" :
							currentFlower = new Flower();
							currentFlower.setGrowingTips(new GrowingTips());
							currentFlower.setVisualParameters(new VisualParameters());
							break;

						case "lighting" :
							String lighting = startElement.getAttributeByName(new QName("lightRequiring")).getValue();
							currentFlower.getGrowingTips().setLighting(lighting);
							break;

						case "name" :
							nextEvent = reader.nextEvent();
							currentFlower.setName(nextEvent.asCharacters().getData());
							break;

						case "soil" :
							nextEvent = reader.nextEvent();
							currentFlower.setSoil(nextEvent.asCharacters().getData());
							break;

						case "origin" :
							nextEvent = reader.nextEvent();
							currentFlower.setOrigin(nextEvent.asCharacters().getData());
							break;

						case "stemColour" :
							nextEvent = reader.nextEvent();
							currentFlower.getVisualParameters().setStemColour(nextEvent.asCharacters().getData());
							break;

						case "leafColour" :
							nextEvent = reader.nextEvent();
							currentFlower.getVisualParameters().setLeafColour(nextEvent.asCharacters().getData());
							break;

						case "aveLenFlower" :
							nextEvent = reader.nextEvent();
							currentFlower.getVisualParameters().setAveLenFlower(nextEvent.asCharacters().getData());
							break;

						case "tempreture" :
							nextEvent = reader.nextEvent();
							currentFlower.getGrowingTips().setTemperature(nextEvent.asCharacters().getData());
							break;

						case "watering" :
							nextEvent = reader.nextEvent();
							currentFlower.getGrowingTips().setWatering(nextEvent.asCharacters().getData());
							break;

						case "multiplying" :
							nextEvent = reader.nextEvent();
							currentFlower.setMultiplying(nextEvent.asCharacters().getData());
							break;

						default:
							break;
					}
				}
				if (nextEvent.isEndElement()) {
					EndElement endElement = nextEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						result.add(currentFlower);
					}
				}
			}

		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<Flower> getResult() {
		return result;
	}
}